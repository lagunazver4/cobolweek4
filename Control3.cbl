       IDENTIFICATION DIVISION.
       PROGRAM-ID.Control3.
       AUTHOR.Michael Coughlan.
       
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  CityCode PIC 9 VALUE ZERO.
           88 CityIsDublin         VALUE 1.
           88 CityIsLimerick       VALUE 2.
           88 CityIsCork           VALUE 3.
           88 CityIsGalway         VALUE 4.
           88 CityIsSligo          VALUE 5.
           88 CityIsWaterford      VALUE 6.
           88 UniversityCity       VALUE 1 THRU 4.
           88 CityCodeNotValid     VALUE 0, 7, 8, 9.

       PROCEDURE DIVISION.
       Begin.
           DISPLAY "Enteracity code(1-6) -  " WITH NO ADVANCING
           ACCEPT CityCode

           IF CityCodeNotValid THEN                                             
             DISPLAY "Invalid city code entered"
           ELSE
              IF CityIsLimerick THEN
                 DISPLAY "Hey, we're home."
              END-IF
              IF CityIsDublin  THEN
                 DISPLAY "Hey, we're in the capital."
              END-IF      
              IF UniversityCity THEN
                 DISPLAY "Apply the rent surcharge!"
              END-IF
           END-IF .
           SET CityIsDublin TO TRUE
           DISPLAY CityCode 
           GOBACK 
           .
